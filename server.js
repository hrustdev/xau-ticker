'use strict';

// application dependencies
var express   = require('express');
var app 	  = express();
var http 	  = require('http');
var server 	  = http.Server(app);
var io		  = require('socket.io')(server);
var cron 	  = require('node-cron');
var BigNumber = require('bignumber.js');

// application constants
var XAURUM_ADDRESS    = '0x4DF812F6064def1e5e029f1ca858777CC98D2D81';
var ETHERSCAN_HOST    = 'api.etherscan.io';
var ETHERSCAN_API_KEY = '2S2BED1QPEPRJYCWGHQRDNI55A256NZ1Z6';
var DIVIDING_NUMBER   =  129600;

// application helper variables
var dividingNumber = new BigNumber(DIVIDING_NUMBER);
var dividingHelper = new BigNumber(100000000);

var tokenSupply;

// listen to port 80
server.listen(80);

// set public folder for static files
app.use(express.static('public'));

// returns index.html file
app.get('/', function (req, res) {
  res.sendfile(__dirname + '/public/index.html');
});

// on new socket connection emits current token supply
io.on('connection', function(socket) {
	if(tokenSupply) {
  		socket.emit('update', { tokenSupply : tokenSupply });
	}
});

// set cron job
cron.schedule('* * * * * *', callEtherscanApi('stats',  'tokensupply', XAURUM_ADDRESS));

/**
 * Create wrapper function which makes etherscan.io API call
 * @param {String} module
 * @param {String} action
 * @param {String} address
 * @return {Function} callEtherscanApi
 */
function callEtherscanApi(module, action, address) {
	var request;
	var options = {
		host: ETHERSCAN_HOST,
		port: 80,
		path: '/api?module=' + module + '&action=' + action + '&contractaddress=' + address + '&apikey=' + ETHERSCAN_API_KEY,
		method: 'GET'
	};

	return function() {
		request = http.request(options, function(res) {
			res.setEncoding('utf8');

			res.on('data', function (rawData) {
				//parse data to object
				var data = JSON.parse(rawData);
				//caculate new token supply by formula
				tokenSupply = new BigNumber(data.result).dividedBy(dividingHelper).dividedBy(dividingNumber).toFixed(8).toString();				
				//emit new token supply to all socket connections
				io.sockets.emit('update', { tokenSupply : tokenSupply });
			});
		});

		// handle http request error
		request.on('error', function(err) {
		});

		// send request
		request.end();	
	}
}