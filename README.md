# [Xaurum] ticker application

## Requirements
Application is using Docker for creating running environment

- [Docker] - Docker is Containers as a Service (CaaS) platform
- [Docker Compose] - Docker Compose is a tool for defining and running multi-container Docker applications

### Running application

Once you have Docker and Docker Compose installed just run following command

```sh
$ docker-compose up
```
[Docker]: <https://www.docker.com>
[Docker Compose]: <https://docs.docker.com/compose/>
[Xaurum]: <http://www.xaurum.org>