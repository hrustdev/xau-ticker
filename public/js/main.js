//connect to socket
var socket = io.connect('127.0.0.1:80');

//on update event updates tokenSupply html element
socket.on('update', function (data) {
	document.getElementById("tokenSupply").innerHTML = data.tokenSupply;
});